package org.example.model;

import lombok.*;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Builder
public class CricketCoach implements Coach{

    private String coachId;
    private String coachName;
    private FortuneService fortuneService;

    public CricketCoach(String coachId, String coachName) {
        this.coachId = coachId;
        this.coachName = coachName;
    }

    @Override
    public String getDailyWorkout() {
        return " Instruction From The Coach=> "+coachName+"  practice spin bowling today";
    }

    @Override
    public String getFortune()
    {
        fortuneService=new HappyFortuneService();
        return "Your Fortune From teh Coach "+coachId+"  "+fortuneService.getDailyFortune();
    }
}
