package org.example.model;

public class HappyFortuneService implements FortuneService{
    @Override
    public String getDailyFortune() {
        return "Hey Today Is Your Lucky Day";
    }
}
